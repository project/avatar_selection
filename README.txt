Drupal Module
------------------------------------------------------------------------------
Name: Avatar Selection module
Author: Stella Power
Drupal: 5.0.x
------------------------------------------------------------------------------

Description
============
When a user edits their accounts details they can choose to upload an image or
photo of themselves, also known as an avatar.  The Avatar Selection module 
allows the user to pick an avatar image from a list already loaded by an 
administrative user.

Configuration
=============
Once the module is activated, go to Administer >> Site configuration >> Avatar
Selection (admin/settings/avatar_selection). Here you'll find a form that will 
allow you to upload or delete images.  Users with the 'administer avatar
selection' access permission will be able to upload and delete the images.

Current maintainer
===================
Stella Power (http://drupal.org/user/66894)
